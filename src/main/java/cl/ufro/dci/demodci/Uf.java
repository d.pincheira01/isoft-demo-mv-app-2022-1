package cl.ufro.dci.demodci;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Uf {
    public Element obtener() throws IOException{

        String url = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
        Document doc = Jsoup.connect(url).get();
        Element content = doc.getElementById("gr");
        Elements value = content.getElementsByClass("obs");
        System.out.println("{" + value.outerHtml() + "}");

        return content;
    }
    public void recorrer(int diaO, String mes) throws IOException {
        String dia = String.valueOf((diaO+1));
        dia = ((dia.length()>1))?dia:"0".concat(dia);
        String idFormat = "grctl"+dia+""+mes;
        Element value = obtener();
        Element uf = value.getElementById(idFormat);
        System.out.println(uf.text());
    }

}
